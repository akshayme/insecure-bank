import jetbrains.buildServer.configs.kotlin.v2019_2.*
import jetbrains.buildServer.configs.kotlin.v2019_2.buildSteps.script
import jetbrains.buildServer.configs.kotlin.v2019_2.triggers.vcs
import jetbrains.buildServer.configs.kotlin.v2019_2.buildFeatures.Swabra
import jetbrains.buildServer.configs.kotlin.v2019_2.buildFeatures.swabra

/*
The settings script is an entry point for defining a TeamCity
project hierarchy. The script should contain a single call to the
project() function with a Project instance or an init function as
an argument.

VcsRoots, BuildTypes, Templates, and subprojects can be
registered inside the project using the vcsRoot(), buildType(),
template(), and subProject() methods respectively.

To debug settings scripts in command-line, run the

    mvnDebug org.jetbrains.teamcity:teamcity-configs-maven-plugin:generate

command and attach your debugger to the port 8000.

To debug in IntelliJ Idea, open the 'Maven Projects' tool window (View
-> Tool Windows -> Maven Projects), find the generate task node
(Plugins -> teamcity-configs -> teamcity-configs:generate), the
'Debug' option is available in the context menu for the task.
*/

version = "2021.1"

fun wrapWithFeature(buildType: BuildType, featureBlock: BuildFeatures.() -> Unit): BuildType {
   buildType.features {
       featureBlock()
   }
   return buildType
}

project {
    buildType(wrapWithFeature(Build){
        swabra {}
    })
}

object Build : BuildType({
    name = "Build"

    vcs {
        root(DslContext.settingsRoot)
    }

    steps {
        script {
            name = "Stage IO"
            scriptContent = """
                rm -fr prescription.sh
                curl -O "https://raw.githubusercontent.com/synopsys-sig/io-artifacts/2021.12.2/prescription.sh"
                sed -i -e 's/\r${'$'}//' prescription.sh
                chmod a+x prescription.sh
                ./prescription.sh \
                    --stage="IO" \
                    --persona="devsecops" \
                    --io.url=%IO_URL% \
                    --io.token=%IO-AUTH-TOKEN% \
                    --manifest.type="json" \
                    --project.name="insecure-bank" \
                    --workflow.url=%WORKFLOW_URL% \
                    --workflow.version=%WORKFLOW_CLIENT_VERSION% \
                    --scm.type="github" \
                    --scm.owner="akshayme-synp" \
                    --scm.repo.name="insecure-bank" \
                    --scm.branch.name="master" \
                    --github.username="akshayme-synp" \
                    --github.token=%Github-AuthToken% \
                    --polaris.project.name="akshayme-synp/insecure-bank" \
                    --polaris.url=%POLARIS_URL% \
                    --polaris.token=%polaris-token% \
                    --blackduck.project.name="insecure-bank:1.0.0" \
                    --blackduck.url=%BLACKDUCK_URL% \
                    --blackduck.api.token=%BlackDuck-AuthToken% \
                    --jira.enable="false" \
                    --IS_SAST_ENABLED="false" \
                    --IS_SCA_ENABLED="false" \
                    --IS_DAST_ENABLED="false"
                mv result.json io-presciption.json
                
                printf "\n"
                
                echo "==================================== IO Risk Score =======================================" > io-risk-score.txt
                echo "Business Criticality Score - ${'$'}(jq -r '.riskScoreCard.bizCriticalityScore' io-presciption.json)" >> io-risk-score.txt
                echo "Data Class Score - ${'$'}(jq -r '.riskScoreCard.dataClassScore' io-presciption.json)" >> io-risk-score.txt
                echo "Access Score - ${'$'}(jq -r '.riskScoreCard.accessScore' io-presciption.json)" >> io-risk-score.txt
                echo "Open Vulnerability Score - ${'$'}(jq -r '.riskScoreCard.openVulnScore' io-presciption.json)" >> io-risk-score.txt
                echo "Change Significance Score - ${'$'}(jq -r '.riskScoreCard.changeSignificanceScore' io-presciption.json)" >> io-risk-score.txt
                export bizScore=${'$'}(jq -r '.riskScoreCard.bizCriticalityScore' io-presciption.json | cut -d'/' -f2)
                export dataScore=${'$'}(jq -r '.riskScoreCard.dataClassScore' io-presciption.json | cut -d'/' -f2)
                export accessScore=${'$'}(jq -r '.riskScoreCard.accessScore' io-presciption.json | cut -d'/' -f2)
                export vulnScore=${'$'}(jq -r '.riskScoreCard.openVulnScore' io-presciption.json | cut -d'/' -f2)
                export changeScore=${'$'}(jq -r '.riskScoreCard.changeSignificanceScore' io-presciption.json | cut -d'/' -f2)
                echo -n "Total Score - " >> io-risk-score.txt && echo "${'$'}bizScore + ${'$'}dataScore + ${'$'}accessScore + ${'$'}vulnScore + ${'$'}changeScore" | bc >> io-risk-score.txt
                
                printf "\n"
                
                cat io-risk-score.txt
                
                echo "IS_SAST_ENABLED = ${'$'}(jq -r '.security.activities.sast.enabled' io-presciption.json)" > io-prescription.txt
                echo "IS_SCA_ENABLED = ${'$'}(jq -r '.security.activities.sca.enabled' io-presciption.json)" >> io-prescription.txt
                echo "IS_DAST_ENABLED = ${'$'}(jq -r '.security.activities.dast.enabled' io-presciption.json)" >> io-prescription.txt
                echo "IS_IMAGE_SCAN_ENABLED = ${'$'}(jq -r '.security.activities.imageScan.enabled' io-presciption.json)" >> io-prescription.txt
                echo "IS_CODE_REVIEW_ENABLED = ${'$'}(jq -r '.security.activities.sastplusm.enabled' io-presciption.json)" >> io-prescription.txt
                echo "IS_PEN_TESTING_ENABLED = ${'$'}(jq -r '.security.activities.dastplusm.enabled' io-presciption.json)" >> io-prescription.txt
                
                printf "\n"
                
                cat io-prescription.txt
            """.trimIndent()
        }
        script {
            name = "Stage WORKFLOW"
            scriptContent = """
                printf "Preparing to run IO Workflow Engine\n"
                
                ./prescription.sh \
                    --stage="WORKFLOW" \
                    --persona="devsecops" \
                    --io.url=%IO_URL% \
                    --io.token=%IO-AUTH-TOKEN% \
                    --manifest.type="json" \
                    --project.name="insecure-bank" \
                    --workflow.url=%WORKFLOW_URL% \
                    --workflow.version=%WORKFLOW_CLIENT_VERSION% \
                    --scm.type="github" \
                    --scm.owner="akshayme-synp" \
                    --scm.repo.name="insecure-bank" \
                    --scm.branch.name="master" \
                    --github.username="akshayme-synp" \
                    --github.token=%Github-AuthToken% \
                    --polaris.project.name="akshayme-synp/insecure-bank" \
                    --polaris.url=%POLARIS_URL% \
                    --polaris.token=%polaris-token% \
                    --blackduck.project.name="insecure-bank:1.0.0" \
                    --blackduck.url=%BLACKDUCK_URL% \
                    --blackduck.api.token=%BlackDuck-AuthToken% \
                    --jira.enable="false" \
                    --IS_SAST_ENABLED="${'$'}(jq -r '.security.activities.sast.enabled' io-presciption.json)" \
                    --IS_SCA_ENABLED="${'$'}(jq -r '.security.activities.sca.enabled' io-presciption.json)" \
                
                printf "\n"
                
                cat synopsys-io.json
                
                printf "\n"
                
                java -jar WorkflowClient.jar --workflowengine.url=%WORKFLOW_URL% --io.manifest.path=synopsys-io.json
            """.trimIndent()
        }
    }

    triggers {
        vcs {
        }
    }
})
